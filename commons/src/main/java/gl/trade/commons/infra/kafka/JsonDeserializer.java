package gl.trade.commons.infra.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;

@Slf4j
public class JsonDeserializer<T> implements Deserializer<T> {
    private final Class<T> targetClass;
    private final ObjectMapper objectMapper;

    public JsonDeserializer(Class<T> targetClass) {
        this.targetClass = targetClass;
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public T deserialize(String topic, byte[] data) {
        try {
            return objectMapper.readValue(data, targetClass);
        } catch (Exception e) {
            log.error("Error serializing data from topic {}: {}", e, topic, data);
        }
        return null;
    }
}
