package gl.trade.commons.functional;

@FunctionalInterface
public interface EffectfulSupplier<T> {
    T get() throws Exception;
}
