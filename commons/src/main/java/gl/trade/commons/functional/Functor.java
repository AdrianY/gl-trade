package gl.trade.commons.functional;

import java.util.function.Function;

@FunctionalInterface
public interface Functor<T> {
    <U> Functor<U> fMap(Function<? super T, ? extends U> mapper);
}
