package gl.trade.commons.functional;

import java.util.function.Function;

public abstract class Either<L, R> {

    public static <T, U> Either<T, U> left(T value){
        return new Left<>(value);
    }

    public static <T, U> Either<T, U> right(U value){
        return new Right<>(value);
    }

    public abstract boolean isLeft();

    public abstract boolean isRight();

    public abstract <NR> Either<L, NR> bind(Function<R, Either<L, NR>> f);

    public abstract <NR> Either<L, NR> map(Function<R, NR> f);

    private static class Left<L, R> extends Either<L, R> {

        private final L value;

        private Left(L value) {
            this.value = value;
        }

        @Override
        public boolean isLeft() {
            return true;
        }

        @Override
        public boolean isRight() {
            return false;
        }

        @Override
        public <NR> Either<L, NR> bind(Function<R, Either<L, NR>> f) {
            return new Left<>(value);
        }

        @Override
        public <NR> Either<L, NR> map(Function<R, NR> f) {
            return new Left<>(value);
        }
    }

    private static class Right<L, R> extends Either<L, R> {

        private final R value;

        private Right(R value) {
            this.value = value;
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        @Override
        public boolean isRight() {
            return true;
        }

        @Override
        public <NR> Either<L, NR> bind(Function<R, Either<L, NR>> f) {
            return f.apply(value);
        }

        @Override
        public <NR> Either<L, NR> map(Function<R, NR> f) {
            return new Right<>(f.apply(value));
        }
    }
}
