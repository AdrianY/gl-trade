package gl.trade.commons.functional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public final class Functional {

    private Functional() throws IllegalAccessException {
        throw new IllegalAccessException("Use as utility");
    }

    public static <A, B> Function<A, B> fromMethod(Function<A, B> f){
        return f;
    }

    public static <A, B> A foldLeft(final Fx2<A, ? super B, A> f, final A initial, final Collection<B> bc){
        return bc.stream().reduce(initial, f::apply, (c, d) -> d);
    }

    public static <A,B> Function<Unit, B> lazyApply(final Function<A, B> f, final A a){
        return unit -> f.apply(a);
    }

    public static <A,B> B foldRight(final Fx2<A, B, B> f, final Collection<A> ac, B b){
        if(ac.size() == 0) {
            return b;
        }
        List<A> reversed = new ArrayList<>(ac);
        Collections.reverse(reversed);
        return foldLeft(Fx2.flip(f), b, reversed);
    }

    public static <A, B, C> Function<Optional<A>, Function<Optional<B>, Optional<C>>> bindOptionals(
            Function<A, Function<B, Optional<C>>> f){
        return a -> b -> a.flatMap(x -> b.flatMap(y -> f.apply(x).apply(y)));
    }
}
