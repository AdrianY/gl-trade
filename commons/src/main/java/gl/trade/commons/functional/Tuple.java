package gl.trade.commons.functional;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Compound data
 *
 * @param <A> First element type
 * @param <B> Second element type
 */
@AllArgsConstructor
@Getter
public class Tuple<A, B> {
    private final A first;
    private final B second;
}
