package gl.trade.commons.functional;

import java.util.function.Function;

/**
 * Function with arity of 3
 * @param <I1> type of first input
 * @param <I2> type of second input
 * @param <I3> type of third input
 * @param <O> type of output
 */
@FunctionalInterface
public interface Fx3<I1, I2, I3, O> {

    /**
     * Execute this function
     * @param inputOne first input
     * @param inputTwo second input
     * @param inputThree third input
     * @return output of this function
     */
    O apply(I1 inputOne, I2 inputTwo, I3 inputThree);

    /**
     * Lower the arity from three to two by
     * partially applying the first input
     *
     * @param inputOne first input
     * @return function of two arity
     */
    default Fx2<I2, I3, O> partial(I1 inputOne) {
        return (i2, i3) -> apply(inputOne, i2, i3);
    }

    /**
     * Lower the arity from three to one by
     * partially applying the first two inputs
     *
     * @param inputOne first input
     * @param inputTwo second input
     * @return function of one arity
     */
    static <A, B, C, D> Function<C, D> partial(Fx3<A, B, C, D> f, A inputOne, B inputTwo) {
        return i3 -> f.apply(inputOne, inputTwo, i3);
    }

    /**
     * Wrap method with three arguments to a
     * function of three arity
     *
     * @param methodReference The method reference
     * @param <A> type of first parameter
     * @param <B> type of second parameter
     * @param <C> type of third parameter
     * @param <D> type of output
     *
     * @return the equivalent function of 3 arity
     */
    static <A, B, C, D> Fx3<A, B, C, D> fromMethod(Fx3<A, B, C, D> methodReference) {
        return methodReference;
    }

    static <A, B, C, D> Fx2<B, C, D> partial1(Fx3<A, B, C, D> f, A inputOne){
        return (i2, i3) -> f.apply(inputOne, i2, i3);
    }

    static <A, B, C, D> Fx2<A, C, D> partial2(Fx3<A, B, C, D> f, B inputTwo){
        return (i1, i3) -> f.apply(i1, inputTwo, i3);
    }

    static <A, B, C, D> Fx2<A, B, D> partial3(Fx3<A, B, C, D> f, C inputThree){
        return (i1, i2) -> f.apply(i1, i2, inputThree);
    }
}
