package gl.trade.commons.functional;

import java.util.function.Function;

public interface IO<T> {

    T run();

    IO<Unit> empty = Unit::unit;

    static <A> IO<A> unit(A a) {
        return () -> a;
    }

    default <B> IO<B> bind(Function<? super T, IO<? extends B>> f) {
        return () -> f.apply(this.run()).run();
    }

    default <B> IO<B> map(Function<? super T, ? extends B> f) {
        return () -> f.apply(this.run());
    }
}
