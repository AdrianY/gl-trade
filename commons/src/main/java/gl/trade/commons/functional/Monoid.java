package gl.trade.commons.functional;

import java.util.Collection;

public interface Monoid<A> {
    Monoid<A> identity();

    Monoid<A> append(final Monoid<A> a);

    default Monoid<A> mconcat(final Collection<? extends Monoid<A>> ac){
        return Functional.foldLeft(Monoid::append, identity(), ac);
    }
}
