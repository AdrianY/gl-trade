package gl.trade.commons.functional;

import java.util.function.Function;

public abstract class Free<T> {

    public static <U> Free<U> liftFree(Functor<? extends U> functor) {
        return null;
    }

    public abstract  <U> Free<U> bind(Function<? super T, ? extends Free<? extends U>> mapper);


}
