package gl.trade.commons.functional;


import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;

public final class Switch<T> {

    private Switch(){}

    public static <U> Switch<U> newInstance(){
        return new Switch<>();
    }

    public <V, U> Case<V, U> caseOf(Predicate<T> condition, Function<T, V> extract, Function<V, U> handler) {
        return new Case<>(condition, extract, handler, null);
    }

    public <V extends T, U> Case<V, U> caseOf(Class<? extends V> clazz, Function<V, U> handler) {
        return new Case<>(clazz::isInstance, clazz::cast, handler, null);
    }

    public <U> Case<T, U> caseOf(Predicate<T> condition, Function<T, U> handler) {
        return new Case<>(condition, identity(), handler, null);
    }

    public class Case<I, O>  {
        private final Predicate<T> patternCondition;
        private final Function<T, I> patternExtraction;
        private final Function<I, O> handler;
        private final Case<?, O> previous;

        private Case(
                final Predicate<T> patternCondition,
                final Function<T, I> patternExtraction,
                final Function<I, O> handler,
                final Case<?, O> previous
        ){
            this.patternCondition = patternCondition;
            this.patternExtraction = patternExtraction;
            this.handler = handler;
            this.previous = previous;
        }

        public Optional<O> apply(T value) {
            if(patternCondition.test(value)) {
                return ofNullable(handler.apply(patternExtraction.apply(value)));
            }
            return ofNullable(previous).flatMap(p -> p.apply(value));
        }

        public <N> Case<N, O> caseOf(Predicate<T> condition, Function<T, N> extract, Function<N, O> map) {
            return new Case<>(condition, extract, map, this);
        }

        public <N extends T> Case<N, O> caseOf(Class<? extends N> clazz, Function<N, O> map) {
            return new Case<>(clazz::isInstance, clazz::cast, map, this);
        }

        public Case<T, O> caseOf(Predicate<T> condition, Function<T, O> map) {
            return new Case<>(condition, identity(), map, this);
        }

        public Function<T, O> otherwise(Function<T, O> map) {
            return value -> this.apply(value)
                    .orElseGet(() -> map.apply(value));
        }
    }
}
