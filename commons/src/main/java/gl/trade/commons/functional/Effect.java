package gl.trade.commons.functional;

/**
 * Used to handle side effects
 */
public interface Effect<T> {

    /**
     * Execute this effect
     * @param t observed type
     */
    void execute(T t);

    default Effect<T> andThen(Effect<T> effect) {
        return b -> {execute(b); effect.execute(b);};
    }
}
