package gl.trade.commons.functional;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableList;

/**
 * A crude writer monad that maintains an internal
 * list that collects values
 * @param <T> type of contained value
 * @param <I> type of items collected
 */
public class Log<T, I> {
    private final T value;
    private final List<I> trace;

    private Log(T value, List<I> newTrace) {
        this.value = value;
        this.trace = newTrace;
    }

    public static <U, T> Log trace(U value, T log) {
        return new Log<>(value, singletonList(log));
    }

    public static <U> Log unit(U value) {
        return new Log<>(value, emptyList());
    }

    public <U> Log<U, I> flatMap(Function<T, Log<U, I>> mapper) {
        Log<U, I> mapped = mapper.apply(value);

        List<I> newTrace =  new ArrayList<>(getTrace());
        newTrace.addAll(mapped.getTrace());

        return new Log<>(mapped.value, newTrace);
    }

    public T getValue() {
        return value;
    }

    public List<I> getTrace() {
        return unmodifiableList(trace);
    }
}
