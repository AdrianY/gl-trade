package gl.trade.commons.functional;

import java.util.function.Function;

/**
 * Function with arity of 2
 * @param <I1> type of first input
 * @param <I2> type of second input
 * @param <O> type of output
 */
@FunctionalInterface
public interface Fx2<I1, I2, O> {

    /**
     * Execute this function
     * @param inputOne first input
     * @param inputTwo second input
     * @return output of this function
     */
    O apply(I1 inputOne, I2 inputTwo);

    /**
     * Flip order of inputs
     * @param fx the original function of arity 2
     * @param <A> first input type
     * @param <B> second input type
     * @param <C> output type
     * @return flipped function of type (B, A) -> C
     */
    static <A, B, C> Fx2<B, A, C> flip(Fx2<A, B, C> fx){
        return (i1, i2) -> fx.apply(i2, i1);
    }

    /**
     * Wrap method with two arguments to a
     * function of one arity
     *
     * @param methodReference The method reference
     * @param <A> type of first parameter
     * @param <B> type of second parameter
     * @param <C> type of output
     *
     * @return the equivalent function of 2 arity
     */
    static <A, B, C> Fx2<A, B, C> fromMethod(Fx2<A, B, C> methodReference) {
        return methodReference;
    }

    static <A, B, C> Function<B, C> partial(Fx2<A, B, C> f, A inputOne){
        return i2 -> f.apply(inputOne, i2);
    }

    static <A, B, C> Function<A, Function<B, C>> curry(Fx2<A, B, C> f){
        return a -> b -> f.apply(a, b);
    }
}
