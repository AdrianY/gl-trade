package gl.trade.commons.utils;

import gl.trade.commons.functional.Result;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;

public class YamlReader {

    public static <T> Result<T> read(Class<? extends T> targetClass, String resourcePath){
        return Result.fallible(() -> {
            Yaml yaml = new Yaml();
            try( InputStream in = targetClass.getClassLoader().getResourceAsStream(resourcePath)) {
                return yaml.loadAs( in, targetClass);
            }
        });

    }
}
