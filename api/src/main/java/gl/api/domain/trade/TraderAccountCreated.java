package gl.api.domain.trade;

import gl.api.domain.DomainEvent;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class TraderAccountCreated implements DomainEvent {

    @Getter(AccessLevel.NONE)
    private final Long version;

    @Getter(AccessLevel.NONE)
    private final String traderId;
    private final String username;
    private final String availableFunds;
    private final String availableCash;

    @Override
    public String getAggregateId() {
        return traderId;
    }

    @Override
    public Long getAggregateVersion() {
        return version;
    }

    @Override
    public String getEventType() {
        return "TRADER_ACCOUNT_CREATED";
    }
}
