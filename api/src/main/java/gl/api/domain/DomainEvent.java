package gl.api.domain;

public interface DomainEvent {
    String getAggregateId();

    Long getAggregateVersion();

    String getEventType();
}