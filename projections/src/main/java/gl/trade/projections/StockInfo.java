package gl.trade.projections;

import lombok.Builder;
import lombok.Value;
import org.javamoney.moneta.Money;

import java.time.LocalDate;

@Value
@Builder
public class StockInfo {
    private final StockId id;
    private final LocalDate date;
    private final Money open;
    private final Money close;
    private final Money high;
    private final Money low;
    private final Long volume;
}
