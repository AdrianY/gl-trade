package gl.trade.projections;

import gl.trade.commons.functional.Fx2;
import gl.trade.commons.functional.Fx3;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public interface StockInfoDAO {

    Function<LocalDate, List<StockInfo>> findAllOnDate();

    Fx2<LocalDate, LocalDate, List<StockInfo>> findAllDuring();

    Fx2<LocalDate, LocalDate, Optional<StockInfo>> findOnDate();

    Fx3<String, LocalDate, LocalDate, List<StockInfo>> findForDuring();

}
