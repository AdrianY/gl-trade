package gl.trade.projections;

import lombok.Builder;
import lombok.Value;
import org.javamoney.moneta.Money;

@Value
@Builder
public class NetForeignValue {
    private final Money value;
    private final Type type;

    public enum Type {
        POSITIVE, NEGATIVE
    }
}
