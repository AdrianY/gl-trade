package gl.trade.domain.commons;

import java.util.Optional;

public interface Command<ID> {
    Optional<ID> aggregateId();
}
