package gl.trade.domain.trader;

import lombok.Value;

@Value
public class Username {
    private final String value;
}
