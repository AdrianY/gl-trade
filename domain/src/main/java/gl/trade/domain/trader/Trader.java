package gl.trade.domain.trader;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;
import lombok.experimental.Wither;
import org.javamoney.moneta.Money;

@Value
@Wither
@Builder(toBuilder = true)
public class Trader {

    @Getter(AccessLevel.NONE)
    private final Long version;

    @Getter(AccessLevel.NONE)
    private final TraderId traderId;

    private final Username username;
    private final Money availableFunds;
    private final Money availableCash;

    public TraderId getId() {
        return traderId;
    }

    public Long getVersion() {
        return version;
    }
}
