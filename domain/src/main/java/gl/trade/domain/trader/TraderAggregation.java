package gl.trade.domain.trader;


import gl.api.domain.DomainEvent;
import gl.api.domain.trade.TraderAccountCreated;
import gl.trade.commons.functional.Functional;
import gl.trade.commons.functional.Fx2;
import gl.trade.commons.functional.Result;
import gl.trade.commons.functional.Switch;
import gl.trade.commons.functional.Tuple;
import gl.trade.domain.commons.Command;

import java.util.UUID;

public final class TraderAggregation {

    private TraderAggregation() throws IllegalAccessException {
        throw new IllegalAccessException("Use statically");
    }

    public static Result<Tuple<Trader, DomainEvent>> processCommand(Command<String> command, Trader trader) {
        return Switch.<Command<String>>newInstance()
                .caseOf(
                        NewAccount.class,
                        Fx2.partial(TraderAggregation::newAccount, trader)
                )
                .otherwise(TraderAggregation::unsupportedCommand)
                .apply(command);
    }

    private static Result<Tuple<Trader, DomainEvent>> newAccount(Trader trader, NewAccount newAccount){
        return Functional.<Tuple<Trader, DomainEvent>, Result<Tuple<Trader, DomainEvent>>>
                fromMethod(Result::success)
                .compose(TraderAggregation::newAccountEvent)
                .compose(Fx2.partial(TraderAggregation::newAccountSnapshot, trader.withTraderId(generateId())))
                .apply(newAccount);
    }

    private static Tuple<Trader, DomainEvent> newAccountEvent(Trader trader){
        return new Tuple<>(
                trader,
                TraderAccountCreated.builder()
                        .availableCash(trader.getAvailableCash().toString())
                        .availableFunds(trader.getAvailableCash().toString())
                        .username(trader.getUsername().getValue())
                        .version(trader.getVersion() + 1L)
                        .traderId(trader.getId().getId())
                        .build()
        );
    }

    private static Trader newAccountSnapshot(
            Trader trader, NewAccount newAccount){
        return trader.toBuilder()
                .availableFunds(newAccount.getInitialFund())
                .availableCash(newAccount.getInitialFund())
                .username(new Username(newAccount.getUsername()))
                .build();
    }

    private static Result<Tuple<Trader, DomainEvent>> unsupportedCommand(Command<String> command){
        return Result.failure(new UnsupportedOperationException("Command not yet supported"+command));
    }

    private static TraderId generateId(){
        return new TraderId(UUID.randomUUID().toString());
    }
}
