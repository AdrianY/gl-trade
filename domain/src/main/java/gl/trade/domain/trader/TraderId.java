package gl.trade.domain.trader;

import lombok.Value;

@Value
public class TraderId {
    private final String id;
}
