package gl.trade.domain.trader;

import gl.api.domain.DomainEvent;
import gl.trade.commons.functional.Fx2;
import gl.trade.commons.functional.Fx3;
import gl.trade.commons.functional.Reader;
import gl.trade.commons.functional.Result;
import gl.trade.commons.functional.Tuple;
import gl.trade.commons.functional.Unit;
import gl.trade.domain.commons.Command;
import lombok.Value;

import java.util.function.Function;

public class TraderService {

    public static Reader<Dependencies, Result<TraderId>> processCommand(Command<String> command){
        return Reader.unit(dependencies ->
                Fx3.partial(TraderService::applyCommand, dependencies, command)
                    .compose(TraderService::resolveSnapshot)
                    .apply(command)
        );
    }

    private static Result<TraderId> applyCommand(
            Dependencies dependencies, Command<String> command, Result<Trader> trader){
        return trader
                .bind(Fx2.partial(TraderAggregation::processCommand, command))
                .bind(repository(dependencies));
    }

    private static Result<Trader> resolveSnapshot(Command<String> command){
        return generateInstance(
                command.aggregateId().map(TraderId::new).orElse(null));
    }

    private static Function<Tuple<Trader, DomainEvent>, Result<TraderId>> repository(
            Dependencies dependencies){
        return trader -> dependencies.saveEvents.apply(trader.getSecond())
                .bind(u -> dependencies.publishEvents.apply(trader.getSecond()))
                .bind(u -> dependencies.persistSnapshot.apply(trader.getFirst()));
    }

    private static Result<Trader> generateInstance(TraderId id){
        return Result.success(Trader.builder().traderId(id).version(0L).build());
    }

    @Value
    public static class Dependencies {
        private Function<? super DomainEvent, Result<Unit>> saveEvents;
        private Function<? super DomainEvent, Result<Unit>> publishEvents;
        private Function<Trader, Result<TraderId>> persistSnapshot;
    }
}
