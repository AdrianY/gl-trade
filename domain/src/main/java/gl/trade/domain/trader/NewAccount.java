package gl.trade.domain.trader;

import gl.trade.domain.commons.Command;
import lombok.Builder;
import lombok.Value;
import org.javamoney.moneta.Money;

import java.util.Optional;

@Value
@Builder
public class NewAccount implements Command<String> {
    private final String username;
    private final Money initialFund;

    @Override
    public Optional<String> aggregateId() {
        return Optional.of(username);
    }
}
