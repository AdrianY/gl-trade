package gl.trade.app.adapters.trader;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Version;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.javamoney.moneta.Money;

@Data
@Entity(value = "trader", noClassnameStored = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TraderSnapshot {

    @Id
    private String id;

    @Version
    private Long version;

    private String username;
    private Money availableFunds;
    private Money availableCash;
}
