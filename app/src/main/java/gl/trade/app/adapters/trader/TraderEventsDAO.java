package gl.trade.app.adapters.trader;

import dev.morphia.Datastore;
import dev.morphia.Key;
import gl.api.domain.DomainEvent;
import gl.api.domain.trade.TraderAccountCreated;
import gl.trade.app.adapters.MongoBasedEvent;
import gl.trade.commons.functional.Fx2;
import gl.trade.commons.functional.Result;
import gl.trade.commons.functional.Switch;
import gl.trade.commons.functional.Unit;

import static java.lang.String.format;

public class TraderEventsDAO {

    public static Result<Unit> save(Datastore datastore, DomainEvent domainEvent) {
        return TraderEventsDAO.convertDomainEvent(domainEvent)
                .bind(Fx2.partial(TraderEventsDAO::persistEvent, datastore))
                .bind(keys -> Result.success());
    }

    private static Result<MongoBasedEvent> convertDomainEvent(DomainEvent domainEvent){
        return Switch.<DomainEvent>newInstance()
                .caseOf(TraderAccountCreated.class, MongoTraderAccountCreated::convert)
                .otherwise(TraderEventsDAO::unsupportedMapping)
                .apply(domainEvent);
    }

    private static Result<MongoBasedEvent> unsupportedMapping(DomainEvent domainEvent){
        return Result.failure(
                new UnsupportedOperationException(
                        format("DomainEvent %s is not yet mapped to any DB equivalent", domainEvent.getClass()
                )));
    }

    private static Result<Key<MongoBasedEvent>> persistEvent(Datastore datastore, MongoBasedEvent event){
        return Result.fallible(() -> datastore.save(event));
    }
}
