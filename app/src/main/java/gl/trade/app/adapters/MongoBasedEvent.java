package gl.trade.app.adapters;

public interface MongoBasedEvent {

    String getId();

    String getAggregateId();

    Long getAggregateVersion();

    String getEventType();
}
