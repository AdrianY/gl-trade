package gl.trade.app.adapters.trader;

import gl.trade.app.context.ServicesContext;
import gl.trade.commons.functional.Fx2;
import gl.trade.domain.trader.TraderService.Dependencies;

public class TraderServiceDependencies {

    public static Dependencies serviceDependencies(ServicesContext servicesContext){
        return new Dependencies(
                Fx2.partial(TraderEventsDAO::save, servicesContext.getDatastore()),
                Fx2.partial(TraderEventsPublisher::publish, servicesContext.getMessagePublisher()),
                Fx2.partial(TraderSnapshotDAO::persist, servicesContext.getDatastore())
        );
    }
}
