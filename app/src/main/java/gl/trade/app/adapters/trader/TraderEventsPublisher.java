package gl.trade.app.adapters.trader;

import gl.api.domain.DomainEvent;
import gl.trade.commons.functional.EffectfulSupplier;
import gl.trade.commons.functional.Functional;
import gl.trade.commons.functional.Fx2;
import gl.trade.commons.functional.Result;
import gl.trade.commons.functional.Unit;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class TraderEventsPublisher {

    public static Result<Unit> publish(
            Producer<String, Object> kafkaProducer, DomainEvent domainEvent) {

        return Functional.<EffectfulSupplier<Unit>, Result<Unit>>
                fromMethod(Result::fallible)
                .compose(Fx2.partial(TraderEventsPublisher::doPublish, kafkaProducer))
                .compose(TraderEventsPublisher::toProducerRecord)
                .apply(domainEvent);
    }

    private static EffectfulSupplier<Unit> doPublish(
            Producer<String, Object> kafkaProducer, ProducerRecord<String, Object> record){
        return () -> {
            kafkaProducer.send(record);
            return Unit.unit();
        };
    }

    private static ProducerRecord<String, Object> toProducerRecord(DomainEvent event){
        return new ProducerRecord<>("gl.domain.trader", event);
    }
}
