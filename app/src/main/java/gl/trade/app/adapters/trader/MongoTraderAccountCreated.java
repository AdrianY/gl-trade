package gl.trade.app.adapters.trader;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import gl.api.domain.trade.TraderAccountCreated;
import gl.trade.app.adapters.MongoBasedEvent;
import gl.trade.commons.functional.Result;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(value = "trader_events", noClassnameStored = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MongoTraderAccountCreated implements MongoBasedEvent {

    @Id
    private String id;
    private String aggregateId;
    private Long aggregateVersion;
    private String username;
    private String availableFunds;
    private String availableCash;
    private String eventType;

    public static Result<MongoBasedEvent> convert(TraderAccountCreated c){
        return Result.success(MongoTraderAccountCreated.builder()
                .aggregateId(c.getAggregateId())
                .aggregateVersion(c.getAggregateVersion())
                .availableCash(c.getAvailableCash())
                .availableFunds(c.getAvailableFunds())
                .eventType(c.getEventType())
                .username(c.getUsername())
                .build()
        );
    }

}
