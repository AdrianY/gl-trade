package gl.trade.app.adapters.trader;

import dev.morphia.Datastore;
import gl.trade.commons.functional.Result;
import gl.trade.domain.trader.Trader;
import gl.trade.domain.trader.TraderId;
import lombok.Value;

@Value
public class TraderSnapshotDAO {

    public static Result<TraderId> persist(Datastore datastore, Trader trader) {
        return Result.fallible(() ->  datastore.save(convert(trader)))
                .map(k -> new TraderId(k.getId().toString()));
    }

    private static TraderSnapshot convert(Trader trader){
        return TraderSnapshot.builder()
                .id(trader.getId().getId())
                .availableCash(trader.getAvailableCash())
                .availableFunds(trader.getAvailableCash())
                .username(trader.getUsername().getValue())
                .version(trader.getVersion())
                .build();
    }
}
