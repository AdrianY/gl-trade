package gl.trade.app;

import gl.trade.app.context.Configuration.HttpServerConfig;
import gl.trade.app.context.ServicesContext;
import gl.trade.app.rest.trader.TraderResource;
import gl.trade.commons.functional.Effect;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class VertxRestVerticle extends AbstractVerticle {

    private final ServicesContext servicesContext;

    public VertxRestVerticle(ServicesContext servicesContext) {
        this.servicesContext = servicesContext;
    }

    @Override
    public void stop(Future future) {
        servicesContext.getMessagePublisher().close();
        future.complete();
    }

    @Override
    public void start(Future future) {
        setupHttpServer(servicesContext.getHttpServerConfig(), vertx, future)
                .andThen(router -> router.route().handler(BodyHandler.create()))
                .andThen(TraderResource.factory(servicesContext))
                .execute(Router.router(vertx));
    }

    private static Effect<Router> setupHttpServer(HttpServerConfig httpServerConfig, Vertx vertx, Future future){
        return router -> vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(httpServerConfig.getPort(), httpServerAsyncResult -> {
                    if (httpServerAsyncResult.succeeded()) {
                        future.complete();
                    } else {
                        future.fail(httpServerAsyncResult.cause());
                    }
                });
    }

}
