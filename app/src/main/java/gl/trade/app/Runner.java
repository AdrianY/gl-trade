package gl.trade.app;

import gl.trade.app.context.Configuration.HttpServerConfig;
import gl.trade.app.context.ServicesContext;
import gl.trade.commons.functional.Result;
import gl.trade.commons.functional.Unit;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import lombok.extern.slf4j.Slf4j;

import static java.lang.String.format;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;

@Slf4j
public class Runner {
    public static void main(String[] args) {
        ServicesContext.createContext(resolveConfigFile(args))
                .bind(Runner::deployVerticle)
                .whenError(t -> log.error("Failed to initialise application", t));
    }

    private static Result<Unit> deployVerticle(ServicesContext servicesContext){
        return Result.fallible(() -> new VertxRestVerticle(servicesContext))
                .map(verticle -> {
                    VertxOptions options = resolveVertxOptions(servicesContext);
                    Vertx.vertx(options).deployVerticle(verticle, completionHandler(servicesContext.getHttpServerConfig()));
                    return Unit.unit();
                });
    }

    private static VertxOptions resolveVertxOptions(ServicesContext servicesContext){
        return new VertxOptions();
    }

    private static Handler<AsyncResult<String>> completionHandler(HttpServerConfig httpServerConfig){
        return result -> {
            if (result.succeeded()){
                    log.info(format("Successfully deployed in port %s", httpServerConfig.getPort()));
            } else {
                    log.error("Failed to deploy", result.cause());
            }
        };
    }

    private static String resolveConfigFile(String... configured){
        if(isEmpty(configured)){
            return "application.yml";
        }

        return configured[0];
    }

}
