package gl.trade.app.rest.trader;

import gl.trade.app.adapters.trader.TraderServiceDependencies;
import gl.trade.app.context.ServicesContext;
import gl.trade.commons.functional.Effect;
import gl.trade.commons.functional.Functional;
import gl.trade.domain.trader.NewAccount;
import gl.trade.domain.trader.TraderId;
import gl.trade.domain.trader.TraderService;
import gl.trade.domain.trader.TraderService.Dependencies;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import static gl.trade.app.rest.ErrorHandling.handleErrorResult;

public final class TraderResource {

    private TraderResource(){}

    public static Effect<Router> factory(ServicesContext servicesContext){
        return Functional.fromMethod(TraderResource::registerRoute)
                .compose(TraderServiceDependencies::serviceDependencies)
                .apply(servicesContext);
    }

    private static Effect<Router> registerRoute(Dependencies dependencies){
        return newTraderAccount(dependencies)
                .andThen(updateTraderAccount(dependencies));
    }

    private static Effect<Router> newTraderAccount(Dependencies dependencies){
        return router -> router.post("/trader").handler(
                routingContext -> Functional.fromMethod(TraderService::processCommand)
                        .compose(TraderResource::newAccountCommand)
                        .apply(routingContext)
                        .apply(dependencies)
                        .whenSuccess(respondAccountCreated(routingContext))
                        .whenError(handleErrorResult(routingContext))
        );
    }

    private static NewAccount newAccountCommand(RoutingContext routingContext){
        return Functional.fromMethod(NewAccountDTO::toDomainCommand)
                .compose((JsonObject j) -> j.mapTo(NewAccountDTO.class))
                .apply(routingContext.getBodyAsJson());
    }

    private static Effect<Router> updateTraderAccount(Dependencies dependencies){
        return router -> router.put("/trader/:id").handler(
                routingContext -> TraderService.processCommand(null)
                        .apply(dependencies)
                        .whenSuccess(respondAccountCreated(routingContext))
                        .whenError(handleErrorResult(routingContext))
        );
    }

    private static Effect<TraderId> respondAccountCreated(RoutingContext routingContext){
        return t -> routingContext.request().response()
                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .setStatusCode(200)
                .setStatusMessage("Created new trader account")
                .end(Json.encodePrettily(t));
    }


}
