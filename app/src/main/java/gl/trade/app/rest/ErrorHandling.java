package gl.trade.app.rest;

import gl.trade.commons.functional.Effect;
import io.vertx.ext.web.RoutingContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ErrorHandling {
    public static Effect<Throwable> handleErrorResult(RoutingContext routingContext){
        return logAsSystemError()
                .andThen(reportAsServerError(routingContext));
    }

    private static Effect<Throwable> logAsSystemError(){
        return t -> log.error("Error occurred", t);
    }

    private static Effect<Throwable> reportAsServerError(RoutingContext routingContext){
        return t -> routingContext.response()
                .setStatusCode(500)
                .setStatusMessage("Internal server error occurred")
                .end();
    }
}
