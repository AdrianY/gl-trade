package gl.trade.app.rest.trader;

import gl.trade.domain.trader.NewAccount;
import lombok.Getter;
import lombok.Setter;
import org.javamoney.moneta.Money;

import javax.money.MonetaryContext;
import javax.money.MonetaryContextBuilder;
import java.math.BigDecimal;

@Getter
@Setter
public class NewAccountDTO {
    private String username;
    private BigDecimal initialFund;
    private String currency;

    public static NewAccount toDomainCommand(NewAccountDTO dto){
        return NewAccount.builder()
                .initialFund(convertAsMoney(dto.getInitialFund(), dto.getCurrency()))
                .username(dto.getUsername())
                .build();
    }

    private static Money convertAsMoney(BigDecimal amount, String currency){
        MonetaryContext moneyContext = MonetaryContextBuilder.of()
                .setFixedScale(true)
                .setMaxScale(2)
                .build();

        return Money.of(amount, currency, moneyContext);
    }
}
