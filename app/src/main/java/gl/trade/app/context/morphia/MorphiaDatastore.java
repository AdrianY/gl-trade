package gl.trade.app.context.morphia;

import com.mongodb.MongoClient;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import gl.trade.app.context.Configuration.DatabaseConnection;
import gl.trade.commons.functional.Result;

import static java.util.Objects.requireNonNull;

public final class MorphiaDatastore {

    public static Result<Datastore> datastore(DatabaseConnection databaseConnection){
        return dbConnection(databaseConnection)
                .bind(MorphiaDatastore::morphia);
    }

    private static Result<MongoClient> dbConnection(DatabaseConnection databaseConnection){
        return Result.fallible(() -> requireNonNull(databaseConnection, "DB Connection properties not found"))
                .map(dC -> new MongoClient(
                        dC.getHost(),
                        dC.getPort()
                ));
    }

    private static Result<Datastore> morphia(MongoClient mongoClient){
        return Result.fallible(() -> {
            final Morphia morphia = new Morphia();
            morphia.mapPackage("gl.trade.app.adapters.trader");
            morphia.getMapper().getConverters().addConverter(new MoneyConverter());
            final Datastore datastore = morphia.createDatastore(mongoClient, "gl-trade");
            datastore.ensureIndexes();
            return datastore;
        });
    }
}
