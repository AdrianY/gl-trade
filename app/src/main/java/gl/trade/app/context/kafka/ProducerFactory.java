package gl.trade.app.context.kafka;

import gl.trade.app.context.Configuration.MessagePublisherConfig;
import gl.trade.commons.functional.Result;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class ProducerFactory {

    public static Result<Producer<String, Object>> createProducer(MessagePublisherConfig messagePublisherConfig) {
        Properties props = producerProperties(messagePublisherConfig);
        return Result.success(new KafkaProducer<>(props, new StringSerializer(), new JsonSerializer()));
    }

    private static Properties producerProperties(MessagePublisherConfig messagePublisherConfig){
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, messagePublisherConfig.getServer());
        properties.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, GLKafkaProducerInterceptor.class.getName());
        return properties;
    }
}
