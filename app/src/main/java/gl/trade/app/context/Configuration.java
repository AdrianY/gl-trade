package gl.trade.app.context;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Configuration {

    private DatabaseConnection databaseConnection;
    private HttpServerConfig httpServerConfig;
    private AppServerConfig appServerConfig;
    private MessagePublisherConfig messagePublisherConfig;

    @Getter
    @Setter
    public static class DatabaseConnection {
        private String host;
        private Integer port;
    }

    @Getter
    @Setter
    public static class HttpServerConfig {
        private Integer port;
    }

    @Getter
    @Setter
    public static class AppServerConfig {

    }

    @Getter
    @Setter
    public static class MessagePublisherConfig {
        private String server;
    }
}
