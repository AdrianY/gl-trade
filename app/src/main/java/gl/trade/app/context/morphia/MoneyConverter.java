package gl.trade.app.context.morphia;

import dev.morphia.converters.TypeConverter;
import dev.morphia.mapping.MappedField;
import org.javamoney.moneta.Money;

import static java.util.Optional.ofNullable;

public class MoneyConverter extends TypeConverter {

    public MoneyConverter(){
        super(Money.class);
    }

    @Override
    public Object decode(Class<?> targetClass, Object fromDBObject, MappedField optionalExtraInfo) {
        return ofNullable(fromDBObject)
                .map(o -> Money.parse(o.toString()))
                .orElse(null);
    }

    @Override
    public Object encode(Object value, MappedField optionalExtraInfo) {
        return ofNullable(value)
                .map(Object::toString)
                .orElse(null);
    }
}
