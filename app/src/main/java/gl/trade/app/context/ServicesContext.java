package gl.trade.app.context;

import dev.morphia.Datastore;
import gl.trade.app.context.Configuration.AppServerConfig;
import gl.trade.app.context.Configuration.HttpServerConfig;
import gl.trade.app.context.kafka.ProducerFactory;
import gl.trade.app.context.morphia.MorphiaDatastore;
import gl.trade.commons.functional.Result;
import gl.trade.commons.utils.YamlReader;
import lombok.Builder;
import lombok.Value;
import org.apache.kafka.clients.producer.Producer;

import static gl.trade.commons.functional.Result.bind3;
import static java.util.Objects.requireNonNull;

@Value
@Builder
public class ServicesContext {
    private final Datastore datastore;
    private final Producer<String, Object> messagePublisher;
    private final HttpServerConfig httpServerConfig;
    private final AppServerConfig appServerConfig;

    public static Result<ServicesContext> createContext(String configurationPath){
        return YamlReader.read(Configuration.class, configurationPath)
            .bind(ServicesContext::generate);
    }

    private static Result<ServicesContext> generate(Configuration configuration){

        return bind3(ServicesContext::generateServiceContext)
                .apply(MorphiaDatastore.datastore(configuration.getDatabaseConnection()))
                .apply(ProducerFactory.createProducer(configuration.getMessagePublisherConfig()))
                .apply(Result.fallible(() -> requireNonNull(
                        configuration.getHttpServerConfig(),
                        "Http Server properties not found"
                )));
    }

    private static Result<ServicesContext> generateServiceContext(
            Datastore datastore,
            Producer<String, Object> messagePublisher,
            HttpServerConfig httpServerConfig
    ){
        return Result.success(
                ServicesContext.builder()
                    .datastore(datastore)
                    .messagePublisher(messagePublisher)
                    .httpServerConfig(httpServerConfig)
                    .build()
        );
    }
}
