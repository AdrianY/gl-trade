package gl.trade.bdd.infra;

import gl.trade.commons.utils.YamlReader;

import static java.util.Objects.nonNull;

public class TestConfigFactory {

    private static TestConfigFactory instance;

    public static TestConfig getInstance(String configurationPath){
        if(nonNull(instance)) {
            return instance.testConfig;
        }

        return YamlReader.read(TestConfig.class, configurationPath)
                .whenSuccess(tC -> instance = new TestConfigFactory(tC))
                .whenErrorThen(tc -> {
                    throw new RuntimeException(tc);
                });
    }

    private final TestConfig testConfig;

    public TestConfigFactory(TestConfig testConfig) {
        this.testConfig = testConfig;
    }
}
