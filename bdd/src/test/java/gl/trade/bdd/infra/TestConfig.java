package gl.trade.bdd.infra;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestConfig {

    private MongoDBConfig mongoDBConfig;
    private AppClient appClient;
    private KafkaConfig kafkaConfig;

    @Getter
    @Setter
    public static class MongoDBConfig {
        private String host;
        private Integer port;
        private String username;
        private String password;
    }

    @Getter
    @Setter
    public static class AppClient {
        private String host;
    }

    @Getter
    @Setter
    public static class KafkaConfig {
        private String bootstrapServer;
    }
}
