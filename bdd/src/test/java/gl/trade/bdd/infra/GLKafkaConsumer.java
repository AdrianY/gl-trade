package gl.trade.bdd.infra;

import gl.trade.bdd.infra.TestConfig.KafkaConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;

@Slf4j
public class GLKafkaConsumer implements Runnable{

    private final AtomicBoolean closed;
    private final Map<String, String> messagesReceived;
    private final Consumer<String, String> consumer;
    private final Function<String, String> idExtractor;

    private GLKafkaConsumer(Consumer<String, String> consumer, Function<String, String> idExtractor) {
        this.messagesReceived = new ConcurrentHashMap<>();
        this.consumer = consumer;
        this.closed  = new AtomicBoolean(false);
        this.idExtractor = idExtractor;
    }

    public static void close(GLKafkaConsumer glKafkaConsumer){
        glKafkaConsumer.closed.set(true);
        glKafkaConsumer.consumer.wakeup();
    }

    public static Optional<String> getValue(GLKafkaConsumer glKafkaConsumer, String key){
        return ofNullable(glKafkaConsumer.messagesReceived.get(key));
    }

    public static GLKafkaConsumer createConsumer(
            KafkaConfig kafkaConfig, String topic, Function<String, String> idExtractor) {
        final Properties props = consumerConfig(kafkaConfig);
        KafkaConsumer<String, String> consumer =
                new KafkaConsumer<>(props, new StringDeserializer(), new StringDeserializer());
        consumer.subscribe(singletonList(topic));
        return new GLKafkaConsumer(consumer, idExtractor);
    }

    private static Properties consumerConfig(KafkaConfig kafkaConfig){
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getBootstrapServer());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "gl-trade-bdd");
        return props;
    }

    @Override
    public void run() {
        try {
            while (!closed.get()) {
                final ConsumerRecords<String, String> consumerRecords =
                        this.consumer.poll(Duration.ofMillis(1000));

                consumerRecords.forEach(record -> {
                    final String message = record.value();
                    log.info("Consumer Record:(%s, %s, %d, %d)\n",
                            record.key(), message,
                            record.partition(), record.offset());

                    messagesReceived.put(idExtractor.apply(message), message);
                });

                consumer.commitAsync();
            }
        } catch (WakeupException e) {
            log.error(e.getMessage(), e);
            if (!closed.get()) throw e;
        } finally {
            consumer.close();
        }
    }
}
