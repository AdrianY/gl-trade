package gl.trade.bdd.infra;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import gl.trade.bdd.infra.TestConfig.MongoDBConfig;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.mongodb.MongoCredential.createCredential;
import static gl.trade.commons.functional.Functional.bindOptionals;
import static java.util.Optional.ofNullable;

public class GLMongoClient {

    private final MongoClient mongoClient;
    private static final String DATABASE_NAME = "gl-trade";

    public static GLMongoClient createMongoClient(MongoDBConfig mongoDBConfig){
        ServerAddress serverAddress = new ServerAddress(mongoDBConfig.getHost(), mongoDBConfig.getPort());

        MongoClient mongoClient = newCredential(mongoDBConfig)
                .map(credential -> new MongoClient(serverAddress, credential, MongoClientOptions.builder().build()))
                .orElseGet(() -> new MongoClient(serverAddress, MongoClientOptions.builder().build()));

        return new GLMongoClient(mongoClient);

    }

    private GLMongoClient(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }


    public Iterable<Document> getDocuments(final String collection) {
        return mongoClient.getDatabase(DATABASE_NAME).getCollection(collection).find();
    }

    public Optional<Document> getDocument(final String collectionName, final Map<String, String> filter) {
        BasicDBObject searchQuery = new BasicDBObject(filter);
        MongoCollection<Document> collection = mongoClient.getDatabase(DATABASE_NAME).getCollection(collectionName);
        return ofNullable(collection.find(searchQuery).first());
    }

    public Optional<Document> getDocumentById(final String collectionName, final String id) {
        Map<String, String> filter = new HashMap<>();
        filter.put("_id", id);
        return getDocument(collectionName, filter);
    }

    private static Optional<MongoCredential> newCredential(MongoDBConfig mongoDBConfig){
        return bindOptionals((String p) -> (String u) ->
                Optional.of(createCredential(u, DATABASE_NAME, p.toCharArray())))
                .apply(ofNullable(mongoDBConfig.getPassword()))
                .apply(ofNullable(mongoDBConfig.getUsername()));
    }
}
