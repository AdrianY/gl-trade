package gl.trade.bdd.commons;

import io.restassured.response.Response;

public interface HttpRestStoryState {
    Response getResponse();
}
