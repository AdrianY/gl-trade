package gl.trade.bdd.commons;

import org.jbehave.core.annotations.Then;

public class RestSteps {

    private final HttpRestStoryState storyState;

    public RestSteps(HttpRestStoryState storyState) {
        this.storyState = storyState;
    }

    @Then("the http response status is $status")
    public void httpResponseStatusIs(int status){
        storyState.getResponse().then()
                .statusCode(status);
    }
}
