package gl.trade.bdd.commons;

import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;
import org.hamcrest.Factory;
import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONCompare;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.JSONCompareResult;

public class JsonMatcher extends DiagnosingMatcher<Object> {

    private final String expectedJSON;
    private JSONCompareMode jsonCompareMode;

    public JsonMatcher(final String expectedJSON) {
        this.expectedJSON = expectedJSON;
        this.jsonCompareMode = JSONCompareMode.NON_EXTENSIBLE;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText(expectedJSON);
    }

    @Override
    protected boolean matches(final Object actual,
                              final Description mismatchDescription) {
        final String actualJSON = toJSONString(actual);
        final JSONCompareResult result;
        try {
            result = JSONCompare.compareJSON(expectedJSON,
                    actualJSON,
                    jsonCompareMode);

            if (!result.passed()) {
                mismatchDescription.appendText(result.getMessage());
            }

            return result.passed();
        } catch (JSONException e) {
            mismatchDescription.appendText(e.getMessage());
            return false;
        }
    }

    private static String toJSONString(final Object o) {
        return o.toString();
    }


    @Factory
    public static JsonMatcher equalToJSON(final String expectedJSON) {
        return new JsonMatcher(expectedJSON);
    }
}