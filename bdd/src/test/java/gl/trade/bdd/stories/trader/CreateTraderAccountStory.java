package gl.trade.bdd.stories.trader;

import gl.trade.bdd.commons.GlJbehaveStory;
import gl.trade.bdd.commons.RestSteps;
import gl.trade.bdd.infra.TestConfig;

public class CreateTraderAccountStory extends GlJbehaveStory {

    @Override
    public Object[] steps(TestConfig testConfig) {

        CreateTraderAccountStoryState state = new CreateTraderAccountStoryState();

        return new Object[]{
                new RestSteps(state),
                new CreateTraderAccountSteps(state, testConfig)};
    }

}