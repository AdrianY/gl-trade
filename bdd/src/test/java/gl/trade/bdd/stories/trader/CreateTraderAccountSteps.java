package gl.trade.bdd.stories.trader;

import gl.trade.bdd.infra.GLKafkaConsumer;
import gl.trade.bdd.infra.GLMongoClient;
import gl.trade.bdd.infra.TestConfig;
import gl.trade.bdd.infra.TestConfig.AppClient;
import io.restassured.specification.RequestSpecification;
import org.bson.Document;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;

import static gl.trade.bdd.commons.JsonMatcher.equalToJSON;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CreateTraderAccountSteps {

    private final GLMongoClient mongoClient;
    private final String traderResource;
    private final GLKafkaConsumer kafkaConsumer;

    private RequestSpecification request;
    private final CreateTraderAccountStoryState state;

    public CreateTraderAccountSteps(CreateTraderAccountStoryState state, TestConfig config) {
        mongoClient = GLMongoClient.createMongoClient(config.getMongoDBConfig());
        traderResource = traderResource(config.getAppClient());
        kafkaConsumer = GLKafkaConsumer.createConsumer(
                config.getKafkaConfig(), "gl.domain.trader", CreateTraderAccountSteps::kafkaMessageIdExtractor);
        this.state = state;
    }

    @BeforeStory
    public void startUp(){
        Executors.newCachedThreadPool().submit(kafkaConsumer);
    }

    @AfterStory
    public void shutdown(){
        GLKafkaConsumer.close(kafkaConsumer);
    }

    @Given("a new trader account with <username>, <initialFund>, and <currency>")
    public void newTraderAccount(
            @Named("username") String username,
            @Named("initialFund") String initialFund,
            @Named("currency") String currency
    ) throws JSONException {
        JSONObject payload = new JSONObject()
                .put("username", username)
                .put("initialFund", initialFund)
                .put("currency", currency);

        request = given().body(payload.toString());
        //create request
    }

    @When("the user requests to create the new trader account")
    public void createTraderAccount() {
        state.setResponse(request.when().post(traderResource));
    }

    @Then("a trader account of snapshot <sVersion>, <sUsername>, <sAvailableCash>, and <sAvailableFund> should exist")
    public void traderAccountSnapshotIsPersisted(
            @Named("sVersion") Long version,
            @Named("sUsername") String username,
            @Named("sAvailableCash") String availableCash,
            @Named("sAvailableFund") String availableFunds
    ) throws JSONException {

        String id = state.getTraderId();
        Optional<Document> trader = mongoClient.getDocumentById("trader", id);

        assertThat(format("Trader account snapshot with id %s does not exist", id),
                trader.isPresent(), equalTo(true));
        assertThat(new JSONObject(trader.get()).toString(), equalToJSON(
                new JSONObject()
                        .put("_id", id)
                        .put("version", version)
                        .put("username", username)
                        .put("availableCash", availableCash)
                        .put("availableFunds", availableFunds)
                .toString()
        ));
    }

    @Then("a trader account created event with details <sVersion>,  <sUsername>, <sAvailableCash>, " +
            "and <sAvailableFund> should exist")
    public void traderAccountCreatedEventIsPersisted(
            @Named("sVersion") Long version,
            @Named("sUsername") String username,
            @Named("sAvailableCash") String availableCash,
            @Named("sAvailableFund") String availableFunds
    ) throws JSONException {
        String id = state.getTraderId();

        Map<String, String> filter = new HashMap<>();
        filter.put("aggregateId", id);
        filter.put("eventType", "TRADER_ACCOUNT_CREATED");
        Optional<Document> trader = mongoClient.getDocument("trader_events", filter);

        assertThat(format("Trader created event for trader with internal id %s does not exist", id),
                trader.isPresent(), equalTo(true));

        assertThat(new JSONObject(trader.map(t -> {
            t.remove("_id");
            return t;
        }).get()).toString(), equalToJSON(
                new JSONObject()
                        .put("aggregateId", id)
                        .put("eventType", "TRADER_ACCOUNT_CREATED")
                        .put("aggregateVersion", version)
                        .put("username", username)
                        .put("availableCash", availableCash)
                        .put("availableFunds", availableFunds)
                        .toString()
        ));
    }

    @Then("a trader account created event with details <sVersion>,  <sUsername>, <sAvailableCash>, " +
            "and <sAvailableFund> is published")
    public void traderAccountCreatedEventIsPublished(
            @Named("sVersion") Long version,
            @Named("sUsername") String username,
            @Named("sAvailableCash") String availableCash,
            @Named("sAvailableFund") String availableFunds
    ) throws JSONException {
        String id = state.getTraderId();
        Optional<String> event = GLKafkaConsumer.getValue(kafkaConsumer, id);
        assertThat(format("Trader created event for trader with internal id %s was published", id),
                event.isPresent(), equalTo(true));

        assertThat(new JSONObject(event.get()), equalToJSON(
                new JSONObject()
                        .put("aggregateId", id)
                        .put("eventType", "TRADER_ACCOUNT_CREATED")
                        .put("aggregateVersion", version)
                        .put("username", username)
                        .put("availableCash", availableCash)
                        .put("availableFunds", availableFunds)
                        .toString()
        ));
    }

    private static String traderResource(AppClient appClient){
        return format("%s/trader", appClient.getHost());
    }

    private static String kafkaMessageIdExtractor(String message){
        try {
            return new JSONObject(message).getString("aggregateId");
        } catch (JSONException e) {
            return null;
        }
    }
}