package gl.trade.bdd.stories.trader;

import gl.trade.bdd.commons.HttpRestStoryState;
import io.restassured.response.Response;
import lombok.Getter;

@Getter
public class CreateTraderAccountStoryState implements HttpRestStoryState {

    private Response response;
    private String traderId;

    public void setResponse(Response response){
        this.response = response;
        this.traderId = response.getBody().jsonPath().getString("id");
    }

}
