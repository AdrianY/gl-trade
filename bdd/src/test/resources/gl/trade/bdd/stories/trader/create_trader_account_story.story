Scenario: when a trader account is created a trader account snapshot and trader account created domainEvent should exist in db

Given a new trader account with <username>, <initialFund>, and <currency>
When the user requests to create the new trader account
Then the http response status is 200
And a trader account of snapshot <sVersion>, <sUsername>, <sAvailableCash>, and <sAvailableFund> should exist
And a trader account created event with details <sVersion>,  <sUsername>, <sAvailableCash>, and <sAvailableFund> should exist
And a trader account created event with details <sVersion>,  <sUsername>, <sAvailableCash>, and <sAvailableFund> is published

Examples:
|username|initialFund|currency|sVersion|sUsername|sAvailableCash|sAvailableFund|
|Pouping |5000       |PHP     |1       |Pouping  |PHP 5000.00   |PHP 5000.00   |
|Poupy   |9998.54    |USD     |1       |Poupy    |USD 9998.54   |USD 9998.54   |